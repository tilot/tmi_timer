#H1 Toastmasters Speech Timer

Autor: tilo.thiele@hamburg.de
Rednerclub Bergedorf

Dies ist eine Stoppuhr für den Clubabend. 

Siehe auch http://www.toastmasters.org


Bedienung:

  
| Rede | maximale zulässige Unterschreitung [sec] |  minimale Länge [min] | maximale Länge [min] | maximale zulässige Überschreitung [sec] |
| ----------- | ----------- | ----------- | ----------- | ----------- |
| Rückmeldezettel | - | 1 | 1 | 0 |
| Stegreifrede (*) | 0 | 1 | 2 | 30 |
| Beobachtungsrede (*) | 30 | 2 | 3 | 30 |
| Eisbrecher (*) | 30 | 4 | 6 | 30 |
| Vorbereitete Rede 1 (*) | 30 | 5 | 7 | 30 |
| Vorbereitete Rede 2 (*) | 30 | 6 | 8 | 30 |
| Vorbereitete Rede 3 (*) | 30 | 7 | 9 | 30 |
| Vorbereitete Rede 4 (*) | 30 | 8 | 10 | 30 |
| Weiterbildungsrede (*) | 30 | 10 | 15 | 30 |
| Profivortrag (*) | 30 | 15 | 20 | 30 |
| Manuell | - | - | - | - |

(*) Diese Redeformate haben (auch) die Möglichkeit, dass nach dem Stoppen der Zeit automatisch in den 'Rückmeldezettel' geschaltet wird (1 Minute Feedbackzettel ausfüllen).