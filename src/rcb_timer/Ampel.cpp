
#include <WString.h>

#include "Ampel.h"
#include "RcbTimerConfig.h"

#include <Adafruit_NeoPixel.h>

extern Adafruit_NeoPixel strip;


const __FlashStringHelper* NeoPixel_Classic::name() {
  return F("Classic");
}

const __FlashStringHelper* NeoPixel_Full::name() {
  return F("Full");
}

const __FlashStringHelper* NeoPixel_Off::name() {
  return F("Off");
}

void NeoPixel_Classic::zeige(int field) {
  uint32_t red_field_color = field==AMPEL_RED ? strip.Color(255, 0, 0) : (uint32_t)0;
  uint32_t yellow_field_color = field==AMPEL_YELLOW ? strip.Color(255, 80, 0) : (uint32_t)0;
  uint32_t green_field_color = field==AMPEL_GREEN ? strip.Color(0, 255, 0) : (uint32_t)0;

  int numStripes = 6;
  int n3 = strip.numPixels()/numStripes;
  int s1 = n3/3;
  int s2 = 2*n3/3;
  long ldr = analogRead(LDR_PIN);
  long k = ldr * ldr / 1024  *255 / 1024;
  if(k<5) k=5;
  if(k>200) k=255;
  
  for(int i=0; i<n3; i++) {
    uint32_t color;
    if(i<s1) {
      color = green_field_color;
    } else if(i<s2) {
      color = yellow_field_color;
    } else {
      color = red_field_color;
    }
    for(int j=0; j<numStripes; j++) {
      if((j%2)==0) {
        // aufsteigender Strang
        strip.setPixelColor(i+n3*j, color);
      } else {
        // absgeigender Strang
        strip.setPixelColor(n3*(j+1)-i-1, color);        
      }
    }
  }
  strip.setBrightness(k);
  strip.show(); 
}


void NeoPixel_Full::zeige(int licht) {
  uint32_t field_color = 0;
  switch(licht) {
    case AMPEL_RED: field_color = strip.Color(255, 0, 0); break;
    case AMPEL_YELLOW: field_color = strip.Color(255, 80, 0); break;
    case AMPEL_GREEN: field_color = strip.Color(0, 255, 0); break;
  }
  
  int n = strip.numPixels();
  long ldr = analogRead(LDR_PIN);
  long k = ldr * ldr / 1024  *255 / 1024;
  if(k<5) k=5;
  
  for(int i=0; i<n; i++) {
      strip.setPixelColor(i, field_color);
  }
  strip.setBrightness(k);
  strip.show(); 
}

void NeoPixel_Off::zeige(int licht) {
  int n = strip.numPixels();
  
  for(int i=0; i<n; i++) {
      strip.setPixelColor(i, 0);
  }
  strip.show(); 
}
