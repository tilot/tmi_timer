#ifndef _AMPEL__H
#define _AMPEL__H


#define AMPEL_AUS 0
#define AMPEL_RED 3
#define AMPEL_YELLOW 2
#define AMPEL_GREEN 1

/*
 * Philips Farbige LED Lampe
 * IR Codes:
 * 
#include <IRremote.h>

IRsend irsend;

unsigned int  red_rawData[67] = {8900,4500, 500,600, 500,600, 500,600, 500,650, 500,600, 500,600, 500,600, 500,650, 450,1750, 500,1700, 550,1700, 450,1750, 550,550, 500,1750, 500,1700, 500,1700, 500,600, 550,600, 550,1650, 550,550, 550,600, 500,600, 500,600, 500,600, 500,1750, 500,1700, 500,600, 500,1700, 550,1700, 500,1700, 500,1750, 500,1700, 500};  // UNKNOWN E85952E1
unsigned int  yello_rawData[67] = {8850,4500, 500,600, 500,600, 500,600, 550,600, 450,650, 500,600, 500,600, 550,600, 500,1700, 500,1700, 500,1750, 500,1700, 500,600, 500,1700, 500,1750, 500,1700, 500,600, 550,550, 500,650, 500,600, 500,1700, 500,600, 550,600, 500,600, 500,1700, 500,1700, 550,1700, 500,1700, 500,650, 500,1700, 500,1750, 500,1700, 500};  // UNKNOWN B0F9B3E1
unsigned int  green_rawData[67] = {8900,4450, 550,550, 500,600, 550,600, 500,600, 500,600, 550,550, 550,600, 450,650, 500,1700, 500,1700, 550,1700, 500,1700, 500,600, 550,1700, 500,1700, 500,1700, 550,1700, 500,600, 500,1700, 500,650, 500,550, 550,600, 500,600, 500,650, 450,650, 450,1750, 450,650, 500,1750, 450,1750, 500,1700, 500,1750, 450,1750, 500};  // UNKNOWN 78CDA4DD
unsigned int  ffff_rawData[3] = {8900,2250, 500};  // NEC FFFFFFFF
unsigned int  off_rawData[67] = {8850,4500, 500,600, 500,600, 500,650, 500,600, 500,600, 500,600, 550,600, 450,650, 500,1700, 500,1750, 500,1700, 500,1750, 450,600, 550,1700, 500,1700, 500,1750, 500,600, 500,1700, 550,600, 500,600, 500,600, 500,600, 500,650, 450,650, 500,1700, 500,600, 500,1750, 450,1750, 500,1700, 550,1700, 450,1750, 500,1700, 550};  // UNKNOWN D4DD0381

void loop() {
  testRaw(red_rawData, sizeof(red_rawData)/sizeof(int));
  testRaw(ffff_rawData, sizeof(ffff_rawData)/sizeof(int));
  delay(3000); //5 second delay between each signal burst
  testRaw(yello_rawData, sizeof(yello_rawData)/sizeof(int));
  testRaw(ffff_rawData, sizeof(ffff_rawData)/sizeof(int));
  delay(3000); //5 second delay between each signal burst
  testRaw(green_rawData, sizeof(green_rawData)/sizeof(int));
  testRaw(ffff_rawData, sizeof(ffff_rawData)/sizeof(int));
  delay(3000); //5 second delay between each signal burst
  testRaw(off_rawData, sizeof(off_rawData)/sizeof(int));
  testRaw(ffff_rawData, sizeof(ffff_rawData)/sizeof(int));
  delay(3000); //5 second delay between each signal burst
}
void testRaw(unsigned int *rawbuf, int rawlen) {
    irsend.sendRaw(rawbuf, rawlen, 38); // 38 KHz
    delay(200);
}   
 * Pin3 -> 330 Ohm -> Bais des Treibertransistors für IR-LED
 * 
 */
class Ampel {

public:
  virtual const __FlashStringHelper* name() = 0;
  virtual void zeige(int licht) = 0;

};

class NeoPixel_Classic : public Ampel {

public:
  const __FlashStringHelper* name();
  void zeige(int licht);

};

class NeoPixel_Full : public Ampel {

public:
  const __FlashStringHelper* name();
  void zeige(int licht);

};

class NeoPixel_Off : public Ampel {

public:
  const __FlashStringHelper* name();
  void zeige(int licht);

};

#endif
