#ifndef DISPLAY_H
#define DISPLAY_H


// wg. DateTime
#include "RTClib.h"

#define FLAG_DT_MI 1
#define FLAG_DT_HH 2
#define FLAG_DT_DD 3
#define FLAG_DT_MM 4
#define FLAG_DT_YY 5


class Display {

public:
  void displayNow(DateTime *shadow_now, int flags, int x, int y);
  
  virtual void restore() = 0;
  virtual void clear() = 0;  
  
  void format_hhmmss(char *buffer, long t);
  void format_hhmmss(char *buffer, int h, int m, int s);

};

class DisplaySet : public Display {
private:
  DateTime shadow_now;
  int shadow_flags;
  int shadow_subject;

  void displayNow();
  void displayLang();
  void displayAmpel();
  void displayTone();

  void cursor();

public:

  void restore();
  
  void clear();

  void now(DateTime *n);
  void subject(int v);
  int subject();
  void lang(int v);
  void ampel(int v);
  void tone(bool v); 
  //void spinner_move_test(int i);
};

class DisplayNormal : public Display {

private:
  byte green_pin, yellow_pin, red_pin, qualified_pin;

  DateTime shadow_now;
  long shadow_previous;
  char shadow_stop_watch_status[13];
  char shadow_speech_type[21];
  long shadow_elapsed_time;
  int shadow_leds;

  void displayElapsedTime();
  void displayStopWatchStatus();
  void displaySpeechType();
  void displayRed();
  void displayGreen();
  void displayYellow();
  void displayQualified();
  void displayPrevious();

  void stripShow(uint32_t color, int field);
  
public:

  DisplayNormal(int green_pin_, int yellow_pin_, int red_pin_, int qualifed_pin_);

  void restore();
  
  void clear();

  void elapsedTime(long elapsed);
  void stopWatchStatus(const char *l);
  void speechType(const char *l);

  void previous(long t);
  
  void red(bool v);
  void yellow(bool v);
  void green(bool v);
  void checkGYRoff();
  
  void qualified(bool v);
  void now(DateTime *n);

};

#endif
