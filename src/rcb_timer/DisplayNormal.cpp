#include "Display.h"
#include "RcbTimerConfig.h"
#include "LanguageResource.h"
#include "Settings.h"
#include "Ampel.h"

extern Settings settings;
extern LanguageResource *resources [];
extern Ampel *ampel [];

#include <LiquidCrystal_I2C.h>
#include <string.h>

extern LiquidCrystal_I2C lcd;

void beeps(int num_beeps);

#define MASK_LEDS_GREEN 1
#define MASK_LEDS_YELLOW 2
#define MASK_LEDS_RED 4
#define MASK_LEDS_QUALIFIED 8

char num2asc(int i) { return '0'+i; }
void fill00(char * buffer, int h, boolean append0) {
  buffer[0] = num2asc(h/10);
  buffer[1] = num2asc(h%10);
  if(append0) {
    buffer[2] = '\0';
  }
}

void fill0000(char * buffer, int h, boolean append0) {
  for(int i=0; i<4; i++) {
    int d = h%10;
    buffer[3-i] = num2asc(d);
    h = h/10;
  }
  if(append0) {
    buffer[4] = '\0';
  }
}

void blinkCursor(int x, int y) {
  lcd.setCursor(x, y);
  lcd.cursor();
  lcd.blink();
}

void noBlinkCursor() {
  lcd.noBlink();
  lcd.noCursor();
}

DisplayNormal::DisplayNormal(int green_pin_, int yellow_pin_, int red_pin_, int qualified_pin_) {
  green_pin = green_pin_;
  yellow_pin = yellow_pin_;
  red_pin = red_pin_;
  qualified_pin = qualified_pin_;
}

void DisplayNormal::clear() {
  lcd.clear();
  shadow_previous = 0;
  shadow_speech_type[0] = shadow_stop_watch_status[0] = '\0';
  shadow_elapsed_time = 0;
  shadow_leds = 0;
}

void DisplayNormal::restore() {
  displayElapsedTime();
  displayStopWatchStatus();
  displaySpeechType();
  displayRed();
  displayGreen();
  displayYellow();
  displayQualified();
  displayNow(&shadow_now, FLAG_DT_YY, 0, 3);
}

// -------------------------------------------------------------------------------------------------
// Display output
// -------------------------------------------------------------------------------------------------


void DisplayNormal::displayElapsedTime() {
  char buffer [] = "--:--:--";
  format_hhmmss(buffer, shadow_elapsed_time);
  lcd.setCursor ( 12, 0 );
  lcd.print(buffer);
}

void rfill(char *s, int len) {
  int l = strlen(s);
  for(int i=l; i<len; i++) s[i] = ' ';
  s[len-1] = '\0';
}

void DisplayNormal::displayStopWatchStatus() {
  lcd.setCursor ( 0, 0 );
  char buffer [sizeof(shadow_stop_watch_status)];
  strcpy(buffer, shadow_stop_watch_status);
  ::rfill(buffer, sizeof(shadow_stop_watch_status));
  lcd.print(buffer);  
}

void DisplayNormal::displaySpeechType() {
  lcd.setCursor ( 0, 1 );
  char buffer [sizeof(shadow_speech_type)];
  strcpy(buffer, shadow_speech_type);
  ::rfill(buffer, sizeof(shadow_speech_type));
  lcd.print(buffer);  
}

void DisplayNormal::displayRed()  {
  bool v = shadow_leds&MASK_LEDS_RED;
  digitalWrite(red_pin, v ? HIGH : LOW);
  if(v) {
    if(settings.tone()) beeps(3);
    ampel[settings.activeAmpel()]->zeige(AMPEL_RED);
  }
}
void DisplayNormal::displayGreen() {
  bool v = shadow_leds&MASK_LEDS_GREEN;
  digitalWrite(green_pin, v ? HIGH : LOW);
  if(v) {
    if(settings.tone()) beeps(1);
    ampel[settings.activeAmpel()]->zeige(AMPEL_GREEN);
  }
}
void DisplayNormal::displayYellow() {
  bool v = shadow_leds&MASK_LEDS_YELLOW;
  digitalWrite(yellow_pin, v ? HIGH : LOW);
  if(v) {
    if(settings.tone()) beeps(2);
    ampel[settings.activeAmpel()]->zeige(AMPEL_YELLOW);
  }
}

void DisplayNormal::displayQualified() {
  bool v = shadow_leds&MASK_LEDS_QUALIFIED;
  digitalWrite(qualified_pin, v ? HIGH : LOW);
}

void DisplayNormal::displayPrevious() {
  lcd.setCursor ( 0, 2 );
  lcd.print(resources[settings.activeResource()]->description(DESCR_PREVIOUS));
  char buffer_hhmmss[10];
  format_hhmmss(buffer_hhmmss, shadow_previous);
  lcd.print(buffer_hhmmss);
}


// ---------------------------------------------------------------
// Setter
// ---------------------------------------------------------------

void DisplayNormal::stopWatchStatus(const char *l) {
  if(strncmp(l, shadow_stop_watch_status, sizeof(shadow_stop_watch_status))) {
    strncpy(shadow_stop_watch_status, l, sizeof(shadow_stop_watch_status));
    displayStopWatchStatus();
  }
}

void DisplayNormal::speechType(const char *l) {
  if(strncmp(l, shadow_speech_type, sizeof(shadow_speech_type))) {
    strncpy(shadow_speech_type, l, sizeof(shadow_speech_type));
    displaySpeechType();
  }
}

void DisplayNormal::elapsedTime(long v) {
  if(shadow_elapsed_time!=v) {
    shadow_elapsed_time = v;
    displayElapsedTime();
  }
}

// LEDs

void DisplayNormal::checkGYRoff() {
  if((shadow_leds & (MASK_LEDS_RED|MASK_LEDS_YELLOW|MASK_LEDS_GREEN))==0) {
    ampel[settings.activeAmpel()]->zeige(AMPEL_AUS);
  }
}

void DisplayNormal::red(bool v) {
  bool old = shadow_leds & MASK_LEDS_RED;
  if(v != old) {
    shadow_leds = v ? (shadow_leds | MASK_LEDS_RED) : (shadow_leds & ~MASK_LEDS_RED);
    displayRed();
    checkGYRoff();
  }
}

void DisplayNormal::yellow(bool v) {
  bool old = shadow_leds & MASK_LEDS_YELLOW;
  if(v != old) {
    shadow_leds = v ? (shadow_leds | MASK_LEDS_YELLOW) : (shadow_leds & ~MASK_LEDS_YELLOW);
    displayYellow();
    checkGYRoff();
  }
}

void DisplayNormal::green(bool v) {
  bool old = shadow_leds & MASK_LEDS_GREEN;
  if(v != old) {
    shadow_leds = v ? (shadow_leds | MASK_LEDS_GREEN) : (shadow_leds & ~MASK_LEDS_GREEN);
    displayGreen();
    checkGYRoff();
  }
}

void DisplayNormal::qualified(bool v) {
  bool old = shadow_leds & MASK_LEDS_QUALIFIED;
  if(v != old) {
    shadow_leds = v ? (shadow_leds | MASK_LEDS_QUALIFIED) : (shadow_leds & ~MASK_LEDS_QUALIFIED);
    displayQualified();
    checkGYRoff();
  }
}

// Strip

void DisplayNormal::previous(long v) {
  if(shadow_previous!=v) {
    shadow_previous = v;
    displayPrevious();
  }
}

void DisplayNormal::now(DateTime *n) {
  int flags = 0;
  if(shadow_now.year()!=n->year()) { flags = FLAG_DT_YY; }
  else if(shadow_now.month()!=n->month()) { flags = FLAG_DT_MM; }
  else if(shadow_now.day()!=n->day()) { flags = FLAG_DT_DD; }
  else if(shadow_now.hour()!=n->hour()) { flags = FLAG_DT_HH; }
  else if(shadow_now.minute()!=n->minute()) { flags = FLAG_DT_MI; } 

  if(flags!=0) {
    shadow_now = *n;
    displayNow(&shadow_now, flags, 0, 3);
  }
}

// -----------------------------------------------------------------------------------------
// Display
// -----------------------------------------------------------------------------------------

// ----+----1----+----2----+----3
// ddd dd.mm.yyyy hh:mi

void Display::displayNow(DateTime *shadow_now, int flags, int x, int y) {
  char buffer[10];
  if(flags >= FLAG_DT_MI) {
    resources[settings.activeResource()]->displayMinute(shadow_now->minute(), x, y);
  }
  if(flags >= FLAG_DT_HH) {
    resources[settings.activeResource()]->displayHour(shadow_now->hour(), x, y);
  }
  if(flags >= FLAG_DT_DD) {
    resources[settings.activeResource()]->displayDay(shadow_now->day(), shadow_now->dayOfTheWeek(), x, y);
  }
  if(flags >= FLAG_DT_MM) {
    resources[settings.activeResource()]->displayMonth(shadow_now->month(), x, y);
  }
  if(flags >= FLAG_DT_YY) {
    resources[settings.activeResource()]->displayYear(shadow_now->year(), x, y);
  }
}

void Display::format_hhmmss(char *buffer, int h, int m, int s) {
  fill00(buffer, h, false);
  buffer[2] = buffer[5] = ':';
  fill00(buffer+3, m, false);
  fill00(buffer+6, s, false);
  buffer[8] = '\0';
}

void Display::format_hhmmss(char *buffer, long t) {
  t = t/1000;
  int s = t % 60; t = t/60;
  int m = t % 60; t = t/60;
  int h = t % 24;
  format_hhmmss(buffer, h, m, s);
}
