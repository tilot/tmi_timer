#include "Display.h"
#include "Ampel.h"

#include <LiquidCrystal_I2C.h>
#include "RcbTimerConfig.h"
#include "Settings.h"
#include "LanguageResource.h"

extern LiquidCrystal_I2C lcd;
extern Settings settings;
extern LanguageResource *resources [];
extern Ampel *ampel [];

Ampel *activeAmpel() { return ampel[settings.activeAmpel()]; }

void blinkCursor(int x, int y);
void noBlinkCursor();


void DisplaySet::clear() {
  lcd.clear();
}

void DisplaySet::restore() {
  lcd.setCursor(0, 0);
  lcd.print(resources[settings.activeResource()]->description(DESCR_SETTINGS_MODE));
  displayNow();
  displayLang();
  displayAmpel();
  displayTone();
}

// -----------------------------------------------------------------------------------------------
// Display
// -----------------------------------------------------------------------------------------------

void DisplaySet::displayNow() {
  int x = 0;
  int y = 1;
  Display::displayNow(&shadow_now, FLAG_DT_MI, x, y);
  Display::displayNow(&shadow_now, FLAG_DT_HH, x, y);
  Display::displayNow(&shadow_now, FLAG_DT_DD, x, y);
  Display::displayNow(&shadow_now, FLAG_DT_MM, x, y);
  Display::displayNow(&shadow_now, FLAG_DT_YY, x, y);
  cursor();
}

void DisplaySet::displayLang() {
  lcd.setCursor(0, 2);
  lcd.print(resources[settings.activeResource()]->description(DESCR_NAME));
  cursor();
}

void DisplaySet::displayTone() {
  lcd.setCursor(10, 2);
  lcd.print(resources[settings.activeResource()]->description(settings.tone() ? DESCR_TONE : DESCR_NO_TONE));
  cursor();
}

void DisplaySet::displayAmpel() {
  lcd.setCursor(0, 3);
  char buffer[20];
  int a = settings.activeAmpel();
  strcpy_P(buffer, (const char*)activeAmpel()->name());
  for(int i=strlen(buffer); i<sizeof(buffer); i++) buffer[i] = ' ';
  buffer[sizeof(buffer)-1] = '\0';
  lcd.print(buffer);
  cursor();
}

void DisplaySet::cursor() {
  switch(shadow_subject) {
    case SET_LANG: blinkCursor(0, 2); return;
    case SET_AMPEL: blinkCursor(0, 3); return;
    case SET_TONE: blinkCursor(10, 2); return;
  }
  blinkCursor(resources[settings.activeResource()]->displayOffset(shadow_subject), 1);
}

// -----------------------------------------------------------------------------------------------
// Setter
// -----------------------------------------------------------------------------------------------

int limit(int v, int max) {
  while(v<0) v+= max;
  return v%max;
}
void DisplaySet::lang(int v) {
  v = limit(v, NUM_LANG_RESOURCES);
  if(v!=settings.activeResource()) {
    settings.activeResource(v);
    lcd.clear();
    restore();
  }
}
void DisplaySet::ampel(int v) {
  v = limit(v, NUM_AMPEL);
  if(v!=settings.activeAmpel()) {
    settings.activeAmpel(v);
    displayAmpel();
  }
}
void DisplaySet::tone(bool v) {
  settings.tone(v);
  displayTone();
}
void DisplaySet::now(DateTime *n) {
  if(shadow_now.year()!=n->year()
    || shadow_now.month()!=n->month()
    || shadow_now.day()!=n->day()
    || shadow_now.hour()!=n->hour()
    || shadow_now.minute()!=n->minute()) {
    shadow_now = *n;
    displayNow();
  }
}

void DisplaySet::subject(int v) {
  bool h = v!=shadow_subject;
  shadow_subject = v;
  if(h) cursor();
}

int DisplaySet::subject() {
  return shadow_subject;
}
