#include "LanguageResource.h"
#include "Settings.h"
#include <HardwareSerial.h>
#include <LiquidCrystal_I2C.h>

extern LiquidCrystal_I2C lcd;

char num2asc(int i);
void fill00(char * buffer, int h, bool append0);
void fill0000(char * buffer, int h, bool append0);

char buffer[21];
const char DESCR_ANY_EMPTY[] PROGMEM  = {""};

// -------------------------------------------------------------------------------
// EN
// -------------------------------------------------------------------------------

const char DESCR_EN_NAME[] PROGMEM  = {"EN"};
const char DESCR_EN_RUNNING[] PROGMEM  = {"running..."};
const char DESCR_EN_PAUSED[] PROGMEM  = {"paused"};
const char DESCR_EN_PREVIOUS[] PROGMEM  = {"previous: "};
const char DESCR_EN_FEEDBACK_SUFFIX[] PROGMEM  = {"+f"};
const char DESCR_EN_SETTING_MODE[] PROGMEM  = {"Settings:"};
const char DESCR_EN_AMPEL[] PROGMEM  = {"stop light"};
const char DESCR_EN_TONE[] PROGMEM  = {"use tone"};
const char DESCR_EN_NO_TONE[] PROGMEM  = {"silent"};

const char* const description_en [] PROGMEM = {
  DESCR_EN_NAME,
  DESCR_EN_RUNNING,
  DESCR_EN_PAUSED,
  DESCR_EN_PREVIOUS,
  DESCR_EN_FEEDBACK_SUFFIX,
  DESCR_ANY_EMPTY,
  DESCR_EN_SETTING_MODE,
  DESCR_EN_AMPEL,
  DESCR_EN_TONE,
  DESCR_EN_NO_TONE
};

const char* LanguageResource_EN::description(unsigned int idx) {
   strcpy_P(buffer, (char*)pgm_read_word(&(description_en[idx]))); 
   return buffer;
}

const __FlashStringHelper* LanguageResource_EN::dayOfTheWeekName(int d) {
  switch(d) {
    case 0: return F("Sun");
    case 1: return F("Mon");
    case 2: return F("Tue");
    case 3: return F("Wed");
    case 4: return F("Thu");
    case 5: return F("Fri");
    case 6: return F("Sat");
  }
  return F("???");
}


const __FlashStringHelper* LanguageResource_EN::speechTypeName(int d) {
  switch(d) {
    case 0: return F("1 feedb");
    case 1: return F("2 feedb");
    case 2: return F("3 feedb");
    case 3: return F("4 tbltpc");
    case 4: return F("5 tbltpc");
    case 5: return F("6 evaln");
    case 6: return F("7 evaln");
    case 7: return F("8 taltl");
    case 8: return F("9 icebr");
    case 9: return F("10 intnl");
    case 10: return F("11 intnl");
    case 11: return F("12 intnl");
    case 12: return F("13 intnl");
    case 13: return F("14 educnl");
    case 14: return F("15 profnl");
    case 15: return F("16 manual");
  }
  return F("???");
}

int LanguageResource_EN::displayOffset(int subject) {
  switch(subject) {
    case SET_MINUTE: return 16;
    case SET_HOUR: return 13;
    case SET_DAY: return 7;
    case SET_MONTH: return 4;
    case SET_YEAR: return 10;
  }
  return 0;
}

void LanguageResource_EN::displayMinute(int d, int x, int y) {
  char buffer[5];
  x+=16;
  lcd.setCursor(x, y);
  fill00(buffer, d, true);
  lcd.print(buffer);
}
void LanguageResource_EN::displayHour(int d, int x, int y) {
  char buffer[5];
  x+=13;
  lcd.setCursor(x, y);
  fill00(buffer, d, true);
  strcat(buffer, ":");
  lcd.print(buffer);
}
void LanguageResource_EN::displayDay(int dayOfMonth, int dayOfWeek, int x, int y) {
  lcd.setCursor(x, y);
  lcd.print(dayOfTheWeekName(dayOfWeek));

  char buffer[5];
  x+=7;
  lcd.setCursor(x, y);
  fill00(buffer, dayOfMonth, true);
  strcat(buffer, "/");
  lcd.print(buffer);
}
void LanguageResource_EN::displayMonth(int d, int x, int y) {
  char buffer[5];
  x+=4;
  lcd.setCursor(x, y);
  fill00(buffer, d, true);
  strcat(buffer, "/");
  lcd.print(buffer);
}
void LanguageResource_EN::displayYear(int d, int x, int y) {
  char buffer[5];
  x+=10;
  lcd.setCursor(x, y);
  fill00(buffer, d%100, true);
  lcd.print(buffer);
}

// -------------------------------------------------------------------------------
// DE
// -------------------------------------------------------------------------------

/*
 \40  !
\41  "
\42  #
\43  $
\44  %
\45  &
\50  (
\51  )
\52  *
\53  Plus-Zeichen
\54  Komma
\55  Minus-Zeichen
\56  Punkt
\57  Schrägstrich
\72  Doppelpunkt
\73  Semikolon
\74  <
\75  Gleichheitszeichen
\76  >
\77  ?
\100  @
\134  eckige Klammer links
\136  eckige Klammer rechts
\137  accent circonflexe
\138  Unterstrich
\140  accent grave
\173  geschweifte Klammer links
\174  senkrechter Strich
\175  geschweifte Klammer rechts
\176  Pfeil nach rechts
\177  Pfeil nach links
\260  Minuszeichen
\333  Kastenrahmen
\337  hochgestellter Kastenrahmen(wie Potenz)
\340  gr. alpha
\341  ä
\342  ß
\343  klein epsilon
\344  µ
\350  Wurzelzeichen
\351  hoch minus 1
\353  hoch x
\356  n mit Oberstrich ( spanisch )
\357  ö
\363  Zeichen unendlich
\364  Ohm
\365  ü
\366  gr.Summe
\367  pi ( klein )
\371  u mit strich rechts unten
\375  geteilt durch
\377  alle Leuchtpunkte eingeschaltet
 */

 /*
  * 225 ä
  * 226 ß
  * 227 €
  * 228 my
  * 239 ö
  * 245 ü
  */
const __FlashStringHelper* LanguageResource_DE::dayOfTheWeekName(int d) {
  switch(d) {
    case 0: return F("So");
    case 1: return F("Mo");
    case 2: return F("Di");
    case 3: return F("Mi");
    case 4: return F("Do");
    case 5: return F("Fr");
    case 6: return F("Sa");
  }
  return F("???");
}


const __FlashStringHelper* LanguageResource_DE::speechTypeName(int d) {
  switch(d) {
    case 0: return F("1 R\365ckmeld");
    case 1: return F("2 R\365ckmeld");
    case 2: return F("3 R\365ckmeld");
    case 3: return F("4 Stegreif");
    case 4: return F("5 Stegreif");
    case 5: return F("6 Beobacht");
    case 6: return F("7 Beobacht");
    case 7: return F("8 Flunkern");
    case 8: return F("9 Eisbrech");
    case 9: return F("10 Internl");
    case 10: return F("11 Internl");
    case 11: return F("12 Internl");
    case 12: return F("13 Internl");
    case 13: return F("14 Weiterb");
    case 14: return F("15 Profess");
    case 15: return F("16 Manuell");
  }
  return F("???");
}

const char DESCR_DE_NAME[] PROGMEM  = {"DE"};
const char DESCR_DE_RUNNING[] PROGMEM  = {"Zeit l\341uft:"};
const char DESCR_DE_PAUSED[] PROGMEM  = {"pausiert"};
const char DESCR_DE_PREVIOUS[] PROGMEM  = {"Letzte: "};
const char DESCR_DE_FEEDBACK_SUFFIX[] PROGMEM  = {"+R"};
const char DESCR_DE_SETTING_MODE[] PROGMEM  = {"Einstellungen:"};
const char DESCR_DE_AMPEL[] PROGMEM  = {"Ampel"};
const char DESCR_DE_TONE[] PROGMEM  = {"mit Beep"};
const char DESCR_DE_NO_TONE[] PROGMEM  = {"ohne Ton"};

const char* const description_de [] PROGMEM = {
  DESCR_DE_NAME,
  DESCR_DE_RUNNING,
  DESCR_DE_PAUSED,
  DESCR_DE_PREVIOUS,
  DESCR_DE_FEEDBACK_SUFFIX,
  DESCR_ANY_EMPTY,
  DESCR_DE_SETTING_MODE,
  DESCR_DE_AMPEL,
  DESCR_DE_TONE,
  DESCR_DE_NO_TONE
};

const char* LanguageResource_DE::description(unsigned int idx) {
   strcpy_P(buffer, (char*)pgm_read_word(&(description_de[idx]))); 
   return buffer;
}

int LanguageResource_DE::displayOffset(int subject) {
  switch(subject) {
    case SET_MINUTE: return 17;
    case SET_HOUR: return 14;
    case SET_DAY: return 3;
    case SET_MONTH: return 6;
    case SET_YEAR: return 9;
  }
  return 0;
}

void LanguageResource_DE::displayMinute(int d, int x, int y) {
  char buffer[5];
  x+=17;
  lcd.setCursor(x, y);
  fill00(buffer, d, true);
  lcd.print(buffer);
}
void LanguageResource_DE::displayHour(int d, int x, int y) {
  char buffer[5];
  x+=14;
  lcd.setCursor(x, y);
  fill00(buffer, d, true);
  strcat(buffer, ":");
  lcd.print(buffer);
}
void LanguageResource_DE::displayDay(int dayOfMonth, int dayOfWeek, int x, int y) {
  lcd.setCursor(x, y);
  lcd.print(dayOfTheWeekName(dayOfWeek));
  char buffer[5];
  x+=3;
  lcd.setCursor(x, y);
  fill00(buffer, dayOfMonth, true);
  strcat(buffer, ".");
  lcd.print(buffer);
}
void LanguageResource_DE::displayMonth(int d, int x, int y) {
  char buffer[5];
  x+=6;
  lcd.setCursor(x, y);
  fill00(buffer, d, true);
  strcat(buffer, ".");
  lcd.print(buffer);
}
void LanguageResource_DE::displayYear(int d, int x, int y) {
  char buffer[5];
  x+=9;
  lcd.setCursor(x, y);
  fill0000(buffer, d, true);
  lcd.print(buffer);
}
