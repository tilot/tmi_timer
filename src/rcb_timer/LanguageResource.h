#ifndef LANGUAGE_RESOURCE_H
#define LANGUAGE_RESOURCE_H

#include <WString.h>

#define DESCR_NAME 0
#define DESCR_RUNNING 1
#define DESCR_PAUSED 2
#define DESCR_PREVIOUS 3
#define DESCR_FEEDBACK_SUFFIX 4
#define DESCR_EMPTY 5
#define DESCR_SETTINGS_MODE 6
#define DESCR_AMPEL 7
#define DESCR_TONE 8
#define DESCR_NO_TONE 9

class LanguageResource {
public:
  virtual const __FlashStringHelper* dayOfTheWeekName(int d) = 0;
  virtual const __FlashStringHelper* speechTypeName(int d) = 0;
  virtual const char* description(unsigned int idx) = 0;
  virtual void displayMinute(int d, int x, int y) = 0;
  virtual void displayHour(int d, int x, int y) = 0;
  virtual void displayDay(int dayOfMonth, int dayOfWeek, int x, int y) = 0;
  virtual void displayMonth(int d, int x, int y) = 0;
  virtual void displayYear(int d, int x, int y) = 0;
  virtual int displayOffset(int subject) = 0;
};

class LanguageResource_EN : public LanguageResource {
public:
  const __FlashStringHelper* dayOfTheWeekName(int d);
  const __FlashStringHelper* speechTypeName(int d);
  const char* description(unsigned int idx);
  void displayMinute(int d, int x, int y);
  void displayHour(int d, int x, int y);
  void displayDay(int dayOfMonth, int dayOfWeek, int x, int y);
  void displayMonth(int d, int x, int y);
  void displayYear(int d, int x, int y);
  int displayOffset(int subject);
};

class LanguageResource_DE : public LanguageResource {
public:
  const __FlashStringHelper* dayOfTheWeekName(int d);
  const __FlashStringHelper* speechTypeName(int d);
  const char* description(unsigned int idx);
  void displayMinute(int d, int x, int y);
  void displayHour(int d, int x, int y);
  void displayDay(int dayOfMonth, int dayOfWeek, int x, int y);
  void displayMonth(int d, int x, int y);
  void displayYear(int d, int x, int y);
  int displayOffset(int subject);
};

#endif
