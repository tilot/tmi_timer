#ifndef MODE_H
#define MODE_H

class Mode {
private:
  virtual void tick() = 0;

public:
  virtual void setup() = 0;
  virtual void loop() = 0;
  virtual void enter() = 0;
  virtual void leave() = 0;

  virtual void startStopClicked() = 0;
  virtual void startStopDblClicked() = 0;
  virtual void startStopLongPressedStop() = 0;
  virtual void ampelBtnClicked() = 0;

};

class NormalMode : public Mode {
private:
  void tick();
  
  int selected_speech_type;
  void refresh();
  void refreshLEDs();
  int lastTimerStatus =-1;

  const char* stop_watch_status();
  const __FlashStringHelper* ampel_status();
  
  void init_timer(int i);

  bool is_feedback_selected();
    
public:
  void setup();
  void loop();
  void enter();
  void leave();

  void ampelBtnClicked();
  void startStopClicked();
  void startStopDblClicked();
  void startStopLongPressedStop();

  // für spinner.moved
  void select_speech_type(int i);
};


class SetMode : public Mode {
private:
  void tick();
  
public:
  void setup();
  void loop();
  void enter();
  void leave();

  void startStopClicked();
  void startStopDblClicked();
  void ampelBtnClicked();
  void startStopLongPressedStop();
};

#endif
