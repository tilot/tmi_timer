
#include <LiquidCrystal_I2C.h>
#include <OneButton.h>
#include <RTClib.h>
#include <Encoder.h>
#include <Adafruit_NeoPixel.h>

#include "RcbTimerConfig.h"
#include "Settings.h"
#include "LanguageResource.h"
#include "Mode.h"
#include "Timer.h"
#include "StopWatch.h"
#include "Display.h"
#include "SpinnerInput.h"

extern Timer rcb_timer;
extern StopWatch stop_watch;
extern RTC_DS1307 rtc;
extern DisplayNormal disp;
extern Adafruit_NeoPixel strip;
extern LiquidCrystal_I2C lcd;
extern Encoder encoder;
extern SpinnerInput spinner_input;
extern Settings settings;
extern LanguageResource *resources [];

extern NormalMode normal_mode;
extern SetMode set_mode;

const speech_type_struct speech_types [] PROGMEM = {
  { false, 60, 1, 1, 0 },
  { false, 120, 2, 2, 0 },
  { false, 300, 5, 5, 0 },
  { false, 0, 1, 2, 30},
  { true, 0, 1, 2, 30},
  { false, 30, 2, 3, 30 },
  { true, 30, 2, 3, 30 },
  { true, 30, 3, 5, 30 },
  { true, 30, 4, 6, 30 },
  { true, 30, 5, 7, 30 },
  { true, 30, 6, 8, 30 },
  { true, 30, 7, 9, 30 },
  { true, 30, 8, 10, 30 },
  { false, 30, 10, 15, 30 },
  { true, 30, 15, 20, 30 },
  { false, 0, 0, 0, 0}
};

int beobachtet_selected = IDX_DEFAULT_TYPE;

// forward decls - warum ist das nötig?
void spinner_moved();
void switchToMode(Mode *m);

extern SpinnerInput spinner_input;


void NormalMode::setup() {
  strip.begin();
  strip.setBrightness(250);
  strip.show(); // Initialize all pixels to 'off'

  selected_speech_type = IDX_DEFAULT_TYPE;
  beobachtet_selected = IDX_DEFAULT_TYPE;
}

unsigned long last_100ms_tick = 0;
unsigned long last_10s_tick = 0;
void NormalMode::loop() {
  unsigned long m = millis();
  if(m>last_100ms_tick+100) {
    last_100ms_tick = m;
    tick();
  }
if(m>last_10s_tick+10000) {
    last_10s_tick = m;
    disp.now(&rtc.now());
  }
  spinner_input.tick();
}


void NormalMode::tick() {
  stop_watch.tick();
  unsigned long t = stop_watch.elapsed_time();
  if(stop_watch.running()) {
    disp.elapsedTime(t);
  }
  refreshLEDs();
}


void NormalMode::enter() {
  spinner_input.init(IDX_DEFAULT_TYPE, 0, NUM_SPEECH_TYPES, 4, &spinner_moved);
  spinner_input.idle(false);
  disp.now(&rtc.now());
  init_timer(selected_speech_type);
  refresh();
}

void NormalMode::leave() {
}

void NormalMode::init_timer(int i) {
  speech_type_struct h;
  memcpy_P(&h, &speech_types[i], sizeof(speech_type_struct));
  rcb_timer.init(String(resources[settings.activeResource()]->speechTypeName(i)).c_str(), h);
  disp.speechType(rcb_timer.name());
}

void spinner_moved() {
  normal_mode.select_speech_type(spinner_input.spinner_value());
}

void NormalMode::select_speech_type(int i) {
  selected_speech_type = i;
  init_timer(selected_speech_type);
}

void NormalMode::ampelBtnClicked() {
  if (rcb_timer.manual() && stop_watch.running()) {
    rcb_timer.proceed_manual_status();
    return;
  }
}

void NormalMode::startStopLongPressedStop() {
  if(!stop_watch.running()) {
    switchToMode(&set_mode);
  }
}

void NormalMode::startStopDblClicked() {
  unsigned long t = stop_watch.elapsed_time();
  if (t != 0 && !is_feedback_selected()) {
    disp.previous(t);
  }

  stop_watch.doStop();
  disp.stopWatchStatus(String(stop_watch_status()).c_str());
  disp.elapsedTime(stop_watch.elapsed_time());
  select_speech_type(selected_speech_type);
  if (rcb_timer.beobachtung()) {
    beobachtet_selected = selected_speech_type;
    select_speech_type(IDX_FEEDBACK_TYPE1);
  } else if (beobachtet_selected != -1 && is_feedback_selected()) {
    select_speech_type(beobachtet_selected);
  }
  spinner_input.idle(stop_watch.running() || stop_watch.elapsed_time() != 0);
}

bool NormalMode::is_feedback_selected() {
  switch(selected_speech_type) {
    case IDX_FEEDBACK_TYPE1:
    case IDX_FEEDBACK_TYPE2:
    case IDX_FEEDBACK_TYPE3:
    return true;
  }
  return false;
}

void NormalMode::startStopClicked() {
  if (stop_watch.running()) {
    stop_watch.doPause();
  } else {
    stop_watch.doStart();
  }
  disp.stopWatchStatus(String(stop_watch_status()).c_str());
  spinner_input.idle(stop_watch.running() || stop_watch.elapsed_time() != 0);
}


void NormalMode::refresh() {
  stop_watch.tick();
  unsigned long t = stop_watch.elapsed_time();
  if(stop_watch.running()) {
    disp.elapsedTime(t);
  }
  disp.restore();
}

const char* NormalMode::stop_watch_status() {
  if(stop_watch.running()) return resources[settings.activeResource()]->description(DESCR_RUNNING);
  else if(stop_watch.elapsed_time()!=0) return resources[settings.activeResource()]->description(DESCR_PAUSED);
  else return resources[settings.activeResource()]->description(DESCR_EMPTY);
}

void NormalMode::refreshLEDs() {
  unsigned long t = stop_watch.elapsed_time();
  int st = rcb_timer.status(t);
  if(st!=lastTimerStatus) {
    lastTimerStatus = st;
    disp.red(rcb_timer.red(st)||rcb_timer.exceeded(st));
    disp.yellow(rcb_timer.yellow(st));
    disp.green(rcb_timer.green(st));
  }
  bool q = false;
  if(rcb_timer.exceeded(st)) {
    long h = millis() % 1000;
    if (h < 0) h = -h;
    q = h > 500;
  } else {
    q = !rcb_timer.manual() && !normal_mode.is_feedback_selected() && !rcb_timer.under(st);
  }
  disp.qualified(q);
}
