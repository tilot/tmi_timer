
#include <HardwareSerial.h>
#include <RTClib.h>

#include "Mode.h"
#include "Display.h"
#include "SpinnerInput.h"
#include "Settings.h"

extern NormalMode normal_mode;
extern SetMode set_mode;
extern Settings settings;
extern DisplayNormal disp;
extern DisplaySet disp_set;
extern RTC_DS1307 rtc;

void switchToMode(Mode *m);
void blinkCursor(int x, int y);
void noBlinkCursor();

extern SpinnerInput spinner_input;

void SetMode::setup() {
}

extern unsigned long last_100ms_tick;
extern unsigned long last_10s_tick;

void SetMode::loop() {
  unsigned long m = millis();
  if(m>last_100ms_tick+100) {
    last_100ms_tick = m;
    tick();
  }
if(m>last_10s_tick+10000) {
    last_10s_tick = m;
    disp_set.now(&rtc.now());
  }
  spinner_input.tick();
}



void SetMode::tick() {
}

/*
void print(const DateTime &dt) {
   Serial.print(dt.year(), DEC);
    Serial.print('/');
    Serial.print(dt.month(), DEC);
    Serial.print('/');
    Serial.print(dt.day(), DEC);
    Serial.print(' ');
    Serial.print(dt.hour(), DEC);
    Serial.print(':');
    Serial.print(dt.minute(), DEC);
    Serial.print(':');
    Serial.println(dt.second(), DEC);
}
*/
void adjust_rtc(const DateTime &t) {
  rtc.adjust(t);
  DateTime n = rtc.now();
  disp_set.now(&n);
}
void adjust_rtc(TimeSpan t) {
  adjust_rtc(rtc.now()+t);
}
void set_spinner_moved() {
  int diff = spinner_input.spinner_diff();
  DateTime n = rtc.now();
  switch(disp_set.subject()) {
    case SET_LANG: 
      disp_set.lang(settings.activeResource()+diff); 
      break;
    case SET_MINUTE: adjust_rtc(TimeSpan(0, 0, diff, 0)); break;
    case SET_HOUR: adjust_rtc(TimeSpan(0, diff, 0, 0)); break;
    case SET_DAY: adjust_rtc(TimeSpan(diff, 0, 0, 0)); break;
    case SET_MONTH: adjust_rtc(TimeSpan(diff*30, 0, 0, 0)); break;
    case SET_YEAR: 
      adjust_rtc(DateTime(n.year()+diff, n.month(), n.day(), n.hour(), n.minute(), n.second())); 
      break;
    case SET_AMPEL:
      disp_set.ampel(settings.activeAmpel()+diff);
      break;
    case SET_TONE:
      disp_set.tone(!settings.tone());
      break;
  }
}

void SetMode::enter() {
  disp_set.subject(SET_AMPEL);
  disp_set.clear();
  disp_set.restore();
  DateTime n = rtc.now();
  disp_set.now(&n);
  spinner_input.init(0, 0, 255, 4, &set_spinner_moved);
  spinner_input.idle(false);
}

void SetMode::leave() {
  disp_set.clear();
  noBlinkCursor();
  settings.writeToEeprom();
}

void SetMode::startStopClicked() {
}

void SetMode::startStopDblClicked() {
}

void SetMode::startStopLongPressedStop() {
  switchToMode(&normal_mode);
}

void SetMode::ampelBtnClicked() {
  int subject = disp_set.subject();
  subject = subject==SET_MAX ? 0 : subject+1;
  disp_set.subject(subject);
  disp_set.restore();
}
