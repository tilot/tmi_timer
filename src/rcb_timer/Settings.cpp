#include "Settings.h"
#include "RcbTimerConfig.h"

#include <EEPROM.h>
#include <HardwareSerial.h>

#define SETTINGS_MAGIC 0xEC

void Settings::writeToEeprom() {
  EEPROM.update(0, SETTINGS_MAGIC);
  int h = flags >> 8;
  EEPROM.update(1, h);
  h = flags & 0xff;
  EEPROM.update(2, h);
}

void Settings::loadFromEeprom() {
  int v = EEPROM.read(0);
  if(v != SETTINGS_MAGIC) {
    return;
  }
  v = EEPROM.read(1);
  v = v<<8;
  v = v|EEPROM.read(2);

  flags = v;
}

// 1.-2. Bit
int Settings::activeResource() {
  return flags & 3;
}

void Settings::activeResource(int v) {
  if(v<0) v=0;
  if(v>=NUM_LANG_RESOURCES) v=NUM_LANG_RESOURCES-1;

  flags &= ~3;
  flags |= (v&3);
}

// 3.-4. Bit
int Settings::activeAmpel() {
  return (flags>>2) & 3;
}

void Settings::activeAmpel(int v) {
  if(v<0) v=0;
  if(v>=NUM_AMPEL) v=NUM_AMPEL-1;

  int mask = 3 << 2;
  flags &= ~mask;
  flags |= ((v<<2)&mask);
}

// 5. Bit
bool Settings::tone() {
  return flags&16;
}

void Settings::tone(bool v) {
  flags = v ? flags|16 : flags&~16;
}
