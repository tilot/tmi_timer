#ifndef SETTINGS_H
#define SETTINGS_H

#define SET_NONE -1
#define SET_YEAR 0
#define SET_MONTH 1
#define SET_DAY 2
#define SET_HOUR 3
#define SET_MINUTE 4
#define SET_LANG 5
#define SET_AMPEL 6
#define SET_TONE 7

#define SET_MAX SET_TONE


class Settings {

  int flags;

public:
  void writeToEeprom();
  void loadFromEeprom();

  int activeResource();
  void activeResource(int v);

  int activeAmpel();
  void activeAmpel(int v);

  bool tone();
  void tone(bool v);
};

#endif
