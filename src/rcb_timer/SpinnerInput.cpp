
#include "SpinnerInput.h"

SpinnerInput::SpinnerInput(Encoder *enc) {
  encoder = enc;
  encoder_position = 0;
}

void SpinnerInput::init(int start_val, int min, int max, int sc, void (*cb)()) {
  value = start_val;
  range_min = min;
  range_max = max;
  scale = sc;
  idle_mode = true;
  spinner_moved_callback = cb;

  position = start_val*sc;
}

void SpinnerInput::tick() {
  int newPosition = encoder->read();
  if (newPosition != encoder_position) {
    diff = newPosition - encoder_position;
    encoder_position = newPosition;

    if(!idle_mode) {
      position += diff;

      int r = range_max - range_min;
      int new_value = (position/scale) % r + range_min;

      while(new_value<range_min) {
        new_value += r;
      }
      
      if(new_value!=value) {
        value = new_value;
        (*spinner_moved_callback)();
      }
    }
  }
}

int SpinnerInput::spinner_value() {
  return value;
}

int SpinnerInput::spinner_diff() {
  return diff;
}

void SpinnerInput::idle(boolean v) {
    if(v!=idle_mode) {

      idle_mode = v;
    }
}
