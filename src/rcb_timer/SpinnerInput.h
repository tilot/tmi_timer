
#ifndef SPINNER_INPUT_H
#define SPINNER_INPUT_H

#include <Encoder.h>

class SpinnerInput {

private:
  Encoder *encoder;
  int encoder_position, position;
  int value, diff;
  int scale, range_min, range_max;
  bool idle_mode;
  void (*spinner_moved_callback)();

public:
  SpinnerInput(Encoder *enc);
  
  void init(int startPosition, int range_min, int range_max, int scale, void (*spinner_moved_callback)());

  void tick();

  void idle(boolean v);

  int spinner_value();
  int spinner_diff();

};

#endif
