#include "StopWatch.h"


StopWatch::StopWatch() {
  init();
}

void StopWatch::init() {
  mode = STOP_WATCH_STOPPED;
  elapsed = 0;
}

unsigned long StopWatch::elapsed_time() {
  return elapsed;
}


bool StopWatch::running() {
  return mode==STOP_WATCH_RUNNING;
  
}

void StopWatch::doStart() {
  if(mode != STOP_WATCH_STOPPED) {
    elapsed = 0;
  }
  tick_time = millis();
  mode = STOP_WATCH_RUNNING;
}

void StopWatch::doPause() {
  tick();
  mode = STOP_WATCH_STOPPED;
}
void StopWatch::doStop() {
  init();
  mode = STOP_WATCH_STOPPED;
}

void StopWatch::tick() {
    if(mode==STOP_WATCH_RUNNING) {
      unsigned long t = millis();
      elapsed += t-tick_time;
      tick_time = t;
    }
}
