#ifndef STOP_WATCH_H
#define STOP_WATCH_H

#include <limits.h>
#include <Arduino.h>

#define STOP_WATCH_UNINITIALIZED 0
#define STOP_WATCH_STOPPED 1
#define STOP_WATCH_RUNNING 2


class StopWatch {

private:
  unsigned long start_time;
  unsigned short mode = STOP_WATCH_UNINITIALIZED;
  unsigned long elapsed;
  unsigned long tick_time;

  void init();
  
public:
  StopWatch();
 
  bool running();

  unsigned long elapsed_time();
  void doStart();
  void doPause();
  void doStop();

  void tick();
};

#endif
