#include "Timer.h"
#include "Settings.h"
#include "LanguageResource.h"

extern Settings settings;
extern LanguageResource *resources [];

#define MILLIS_IN_MINUTE 60000;

void Timer::init(const char *name_, speech_type_struct typ) {
  init(name_, typ.mit_beobachtung, typ.pre, typ.min, typ.max, typ.post);
}

void Timer::init(const char *name_, bool mit_beobachtung_, long pre_, long min_time_, long max_time_, long post_) {
  pre = pre_*1000;
  post = post_*1000;
  min_time = min_time_*MILLIS_IN_MINUTE;
  max_time = max_time_*MILLIS_IN_MINUTE;
  mit_beobachtung = mit_beobachtung_;
  String n = String(name_);
  if(mit_beobachtung) {
    n += resources[settings.activeResource()]->description(DESCR_FEEDBACK_SUFFIX);
  }
  if(min_time_!=0) {
    n+= F(" (");
    n+= min_time_;
    if(min_time_!=max_time_) {
      n+= F("-");
      n+= max_time_;
    }
    n+= F(")");
  }
  n.toCharArray(timer_name, sizeof(timer_name));
  manual_status = STATUS_UNDER;
}

bool Timer::beobachtung() {
  return mit_beobachtung;
}

void Timer::proceed_manual_status() {
  if(manual_status==STATUS_UNDER)
      manual_status = STATUS_GREEN;
  else if(manual_status==STATUS_GREEN)
      manual_status = STATUS_YELLOW;
  else if(manual_status==STATUS_YELLOW)
      manual_status = STATUS_RED;
  else if(manual_status==STATUS_RED)
      manual_status = STATUS_UNDER;
}

bool Timer::green(unsigned int st) {
  return st==STATUS_GREEN;
}

bool Timer::yellow(unsigned int st) {
  return st==STATUS_YELLOW;
}
bool Timer::red(unsigned int st) {
  return st==STATUS_RED;
}

bool Timer::exceeded(unsigned int st) {
  return st==STATUS_DISQUALIFIED;
}

bool Timer::under(unsigned int st) {
  return st==STATUS_UNDER;
}

unsigned long Timer::yellow_time() {
  return (min_time+max_time)/2;
}

unsigned long Timer::exceeded_time() {
  if(min_time==0) return 0;
  return max_time+post;
}

unsigned long Timer::under_time() {
  long rv = min_time-pre;
  return rv<0 ? 0 : rv;
}

const char* Timer::name() {
  return timer_name;
}

bool Timer::manual() {
  return max_time==0;
}


unsigned int Timer::status(unsigned long elapsed_millis) {
  if(manual()) return manual_status;
  if(elapsed_millis<under_time()) return STATUS_UNDER;
  if(elapsed_millis<min_time) return STATUS_QUALIFIED;
  if(elapsed_millis<yellow_time()) return STATUS_GREEN;
  if(elapsed_millis<max_time) return STATUS_YELLOW;
  if(elapsed_millis<exceeded_time()) return STATUS_RED;
  return STATUS_DISQUALIFIED;
}





  
