
#ifndef TIMER_H
#define TIMER_H


#include <limits.h>
#include <Arduino.h>

struct speech_type_struct {
  bool mit_beobachtung; // true = anschließend die 1-Minute für die Rückmeldung
  long pre; // Anzahl Sekunden vor der Minimun-Zeit ab der die Rede gültig ist - Reden, die kürzer sind werden disqualifiziert
  long min; // Minimale Redezeit in Minuten (ab dann ist Grün)
  long max; // Maximale Redezeit in Minuten (ab dann ist Rot)
  long post; // Anzahl Sekunden nach Erreichen der Maximalen Redezeit bis die Rede disqualifiziert ist.
};

#define STATUS_UNDER 0
#define STATUS_QUALIFIED 1
#define STATUS_GREEN 2
#define STATUS_YELLOW 3
#define STATUS_RED 4
#define STATUS_DISQUALIFIED 5

class Timer {

private:
  long pre, post;
  long min_time;
  long max_time;
  char timer_name[20];
  bool mit_beobachtung;
  int manual_status;

  unsigned long yellow_time();
  unsigned long under_time();
  unsigned long exceeded_time();



public:
  void init(const char *name, speech_type_struct typ);
  void init(const char *name, bool mit_beobachtung_, long pre_, long min_time_, long max_time_, long post_);

  void proceed_manual_status();
  bool manual();
  
  const char *name();

  unsigned int status(unsigned long elapsed_millis);

  bool green(unsigned int status);
  bool yellow(unsigned int status);
  bool red(unsigned int status);
  bool under(unsigned int status);
  bool exceeded(unsigned int status);

  bool beobachtung();
};

#endif
