
/*
 * Aus SPEECH CONTEST RULEBOOK, JULY 1, 2018 TO JUNE 30, 2019
 * 
6. 
Timing of the Speeches
A. 
Two timers are appointed by the chief judge. One is provided with a stopwatch, and 
the other with a signaling device that displays green, yellow, and red colors.
B. 
The signaling device must be in full view of each contestant.
C. 
The timer with the stopwatch maintains and delivers to the chief judge the written 
record of elapsed time of each speech on the Speech Contest Time Record Sheet and 
Instructions for Timers (Item 1175).
D. 
The timer managing the timing device ensures that contestants are able to view an 
accurate green, yellow, or red signal at appropriate times during the speech.
E. 
All speeches delivered by contestants must conform to the timing guidelines for
the contest.
1. 
International and Humorous speeches shall be from five to seven minutes.
A contestant will be disqualified if the speech is less than four minutes 30 seconds 
or more than seven minutes 30 seconds.
2. 
Table Topics speeches shall be from one minute to two minutes. A  
contestant will 
be disqualified if the speech is less than one minute or more than two minutes 
30 seconds.
3. 
Evaluation speeches shall be from two to three minutes. A contestant will be 
disqualified if the speech is less than one minute 30 seconds or more than three 
minutes 30 seconds.
4. 
Tall Tales speeches shall be from three to five minutes. A contestant will be 
disqualified if the speech is less than two minutes 30 seconds or more than five 
minutes 30 seconds.
F.   
Upon being introduced, the contestant shall proceed immediately to the speaking 
position.
1. 
Timing will begin with the contestant’s first definite verbal or nonverbal com
-
munication with the audience. This usually will be the first word uttered by the 
contestant, but would include any other communication such as sound effects, 
a staged act by another person, etc.
2. 
The speaker should begin speaking within a short time after arriving at the 
speaking area, and is not permitted to delay the contest unnecessarily.
G. 
Timers shall provide warning signals to the contestants, which shall be clearly visible 
to the speakers but not obvious to the audience.
1. 
For International and Humorous contests:
a) 
A green signal will be displayed at five minutes and remain displayed for 
one minute.
b) 
A yellow signal will be displayed at six minutes and remain displayed for 
one minute.
c) 
A red signal will be displayed at seven minutes and will remain on until the 
conclusion of the speech.
14
2. 
For Evaluation contests:
a) 
The green signal will be displayed at two minutes and remain displayed for 
30 seconds. 
b) 
The yellow signal will be displayed at two minutes and 30 seconds and remain 
displayed for 30 seconds.
c) 
The red signal will be displayed at three minutes and remain displayed until the 
evaluation is concluded.
3. 
For Table Topics contests:
a) 
The green signal will be displayed at one minute and remain displayed for 
30 seconds.
b) 
The yellow signal will be displayed at one minute 30 seconds and remain 
displayed for 30 seconds.
c) 
The red signal will be displayed at two minutes and remain displayed until the 
speech is concluded.
4. 
For Tall Tales contests:
a) 
The green signal will be displayed at three minutes and remain displayed for 
one minute.
b) 
The yellow signal will be displayed at four minutes and remain displayed for 
one minute. 
c) 
The red signal will be displayed at five minutes and remain displayed until the 
speech is concluded.
5. 
In all speech contests, no signal shall be given for the overtime period.
6. 
Any visually impaired contestant is permitted to request and must be granted a 
form of warning signal of his or her own choosing.
a) 
Acceptable warning signals would include, but not be limited to: a buzzer, a bell, 
or a person announcing the times at five, six, and seven minutes.
b) 
If any special device and/or specific instructions for such signal is/are required, 
the contestant must provide same.
7. 
In the event of technical failure of the signal or timing equipment, a speaker is 
allowed 30 seconds extra overtime before being disqualified.
H. 
Prior to announcing results, the contest chair shall announce if time disqualifications 
occurred, but not name the contestant(s) involved.
 */

// Benötigte Libraries:
// Adafruit BusIO 1.14.1
// Adafruit NeoPixel 1.11.0
// Encoder 1.4.2
// LiquidCrystal I2C 1.1.2
// OneButton 2.0.4
// RTClib 2.1.1

// Gebaut mit: Arduino Nano
// Angeschlossen an /dev/ttyUSB0
// Processor: ATMega328P (Old Bootloader)
// Programmer: Arduino as ISP

// Ausgabe des Compilers:
// Der Sketch verwendet 22950 Bytes (74%) des Programmspeicherplatzes. Das Maximum sind 30720 Bytes.
// Globale Variablen verwenden 1084 Bytes (52%) des dynamischen Speichers, 964 Bytes für lokale Variablen verbleiben. Das Maximum sind 2048 Bytes.

 /*
  * 2004 LCD Display via I2C
  * LiquidCrystal I2C Display
  * http://wiki.sunfounder.cc/index.php?title=I2C_LCD2004#Step_2:Add_library
  */

  /*
   * Ky-040 Dreheinsteller
   *    SW -> D10 (10K Pull-Up bereits auf dem Modul)
   *    CLK -> D9 (10K Pull-Up bereits auf dem Modul)
   *    DT -> D8 (10K Pull-Up bereits auf dem Modul)
   *    
   * I2C (LCD-Display)
   *    SDA -> A4 (mit 10K Pull-Up)
   *    SCL -> A5 (mit 10K Pull-Up)
   *    
   * Start/Stop Button (NO mit 10K Pull-Down)
   *    -> an D2 
   *    
   * LEDs mit 220 Ohm Vorwiderstand
   *    Blau -> D4
   *    Grün -> D5
   *    Gelb -> D6
   *    Rot -> D7
   *        
   *  LDR -> A0 mit 10K Pull-Down Widerstand
   *       
   * WS28B 60er RGB LED-Streifen
   *    DI -> D3 (über 220 Ohm Vorwiderstand)
   */
  
#define ENCODER_OPTIMIZE_INTERRUPTS
#include <Encoder.h>

#include <LiquidCrystal_I2C.h>
#include <OneButton.h>
#include <RTClib.h>
#include <Adafruit_NeoPixel.h>

#include "RcbTimerConfig.h"
#include "Mode.h"
#include "Timer.h"
#include "StopWatch.h"
#include "Display.h"
#include "SpinnerInput.h"
#include "LanguageResource.h"
#include "Settings.h"
#include "Ampel.h"

NormalMode normal_mode;
SetMode set_mode;

Mode* active_mode = &normal_mode;

Timer rcb_timer;
StopWatch stop_watch;
RTC_DS1307 rtc;
DisplayNormal disp(GREEN_PIN, YELLOW_PIN, RED_PIN, QUALIFIED_PIN);
DisplaySet disp_set;
Adafruit_NeoPixel strip = Adafruit_NeoPixel(60, STRIP_PIN, NEO_GRB + NEO_KHZ800);
LiquidCrystal_I2C lcd(0x27,20,4);
Encoder encoder(8,9);
SpinnerInput spinner_input(&encoder);
LanguageResource_EN en;
LanguageResource_DE de;
LanguageResource *resources [NUM_LANG_RESOURCES] = {&de, &en};
NeoPixel_Classic ampel_classic;
NeoPixel_Full ampel_full;
NeoPixel_Off ampel_off;
Ampel *ampel [NUM_AMPEL] = {&ampel_classic, &ampel_full, &ampel_off};
Settings settings;

OneButton startStoppButton(START_STOP_BUTTON_PIN, false);
OneButton ampelButton(TYPE_SELECT_BUTTON_PIN, true);

void setup() {
  Serial.begin(57600);

  /* Set the pin modes */
  pinMode(QUALIFIED_PIN, OUTPUT);
  pinMode(GREEN_PIN, OUTPUT);
  pinMode(YELLOW_PIN, OUTPUT);
  pinMode(RED_PIN, OUTPUT);
  pinMode(TYPE_SELECT_BUTTON_PIN, INPUT_PULLUP);
  pinMode(START_STOP_BUTTON_PIN, INPUT);

  settings.loadFromEeprom();
  
  startStoppButton.attachClick(startStopClicked);
  startStoppButton.attachDoubleClick(startStopDblClicked);
  startStoppButton.attachLongPressStart(startStopLongPressedStop);
  ampelButton.attachClick(ampelBtnDblClicked);

  rtc.begin();
  lcd.init();  //initialize the lcd
  lcd.backlight();  //open the backlight 
  lcd.setCursor ( 0, 0 );

  if(rtc.now().year()==2000) {
    // muss offenbar eingestellt werden.
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }
 
  normal_mode.setup();
  set_mode.setup();
  normal_mode.enter();
}

unsigned long last_millis_buttons = 0;

void loop() { 
  unsigned long m = millis();
  if(m+10>last_millis_buttons) {
    last_millis_buttons = m;
    startStoppButton.tick();
    ampelButton.tick();
  }
  active_mode->loop();
  beep_tick();
}

void switchToMode(Mode *m) {
  active_mode->leave();
  active_mode = m;
  active_mode->enter();
}

void startStopLongPressedStop() {
  active_mode->startStopLongPressedStop();
}

void startStopClicked() {
  active_mode->startStopClicked();
}

void startStopDblClicked() {
  active_mode->startStopDblClicked();
}

void ampelBtnDblClicked() {
    active_mode->ampelBtnClicked();
}

// -------------------------------------------------------------------------------------
// beep
// -------------------------------------------------------------------------------------
int num_beeps;
long tone_start;

void beeps(int num) {
  num_beeps = num;
  if(num!=0) {
    tone_start = millis();
    if(num_beeps<0) num_beeps = -num_beeps;
    tone(TONE_PIN, TONE_FREQUENCY);
  } else {
    noTone(TONE_PIN);
  }
}

bool shadow_tone_on() {
  return num_beeps>0;
}

void shadow_tone_on(bool v) {
  if(num_beeps<0) num_beeps = -num_beeps;
  if(!v) num_beeps = -num_beeps;
}

void beep_tick() {
  if(num_beeps!=0) {
    int nb1 = num_beeps<0 ? -num_beeps : num_beeps;
    long n = millis();
    int beep_len = (1000+(nb1-1)*200) / nb1;
    long h = (n- tone_start) % (beep_len + BEEP_GAP);
    long nb = (n- tone_start) / (beep_len + BEEP_GAP);
    bool on;
    if(nb==nb1) {
      beeps(0);
      return;
    } else {
      on = h<beep_len;
    }
    if(on!=shadow_tone_on()) {
      shadow_tone_on(on);
      if(on) tone(TONE_PIN, TONE_FREQUENCY);
      else noTone(TONE_PIN);
    }
  }
}
